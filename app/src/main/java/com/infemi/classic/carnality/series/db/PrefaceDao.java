package com.infemi.classic.carnality.series.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.infemi.classic.carnality.series.db.entity.Preface;

import java.util.List;

/**
 * Created by TheDancerCodes on 18/10/2018.
 */
@Dao // This annotation defines this interface as a DAO interface.
public interface PrefaceDao {

    @Insert

    // The return type of this method should be int.
    public long addPrefaceText(Preface preface);


    @Update
    public void updatePreface(Preface preface);

    @Delete
    public void deletePreface(Preface preface);

    @Query(" select * from preface")

    // The return type of this method should a list of Preface objects.
    public List<Preface> getPrefaceEntries();

    @Query("select * from preface where preface_id ==:prefaceId")
    public Preface getPrefaceEntry(long prefaceId);

}
