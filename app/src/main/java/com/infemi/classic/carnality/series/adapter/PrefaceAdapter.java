package com.infemi.classic.carnality.series.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.infemi.classic.carnality.series.R;
import com.infemi.classic.carnality.series.StudyMainActivity;
import com.infemi.classic.carnality.series.db.entity.Preface;

import java.util.ArrayList;

/**
 * Created by TheDancerCodes on 19/10/2018.
 */
public class PrefaceAdapter extends RecyclerView.Adapter<PrefaceAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Preface> prefaceList;
    private StudyMainActivity studyMainActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView text;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            text = view.findViewById(R.id.text);

        }
    }


    public PrefaceAdapter(Context context, ArrayList<Preface> prefaces, StudyMainActivity mainActivity) {
        this.context = context;
        this.prefaceList = prefaces;
        this.studyMainActivity = mainActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.preface_list_item, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        final Preface preface = prefaceList.get(position);

        holder.title.setText(preface.getTitle());
        holder.text.setText(preface.getText());

        holder.itemView.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                studyMainActivity.addAndEditPreface(true, preface, position);
            }
        });

    }

    @Override
    public int getItemCount() {

        return prefaceList.size();
    }

}
