package com.infemi.classic.carnality.series;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.infemi.classic.carnality.series.adapter.PrefaceAdapter;
import com.infemi.classic.carnality.series.db.CarnalitySeriesDatabase;
import com.infemi.classic.carnality.series.db.entity.Preface;

import java.util.ArrayList;

/**
 * Created by TheDancerCodes on 19/10/2018.
 */
public class StudyMainActivity extends AppCompatActivity {

    private PrefaceAdapter prefaceAdapter;
    private ArrayList<Preface> prefaceArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CarnalitySeriesDatabase carnalitySeriesDatabase;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("The Preface");

        recyclerView = findViewById(R.id.recycler_view_preface);

        carnalitySeriesDatabase = Room.databaseBuilder(getApplicationContext(), CarnalitySeriesDatabase.class, "CarnalityStudyDB").allowMainThreadQueries().build();

        prefaceArrayList.addAll(carnalitySeriesDatabase.getPrefaceDao().getPrefaceEntries());

        prefaceAdapter = new PrefaceAdapter(this, prefaceArrayList, StudyMainActivity.this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(prefaceAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAndEditPreface(false, null, -1);
            }


        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        long id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addAndEditPreface(final boolean isUpdate, final Preface preface, final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.layout_add_preface, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(StudyMainActivity.this);
        alertDialogBuilderUserInput.setView(view);

        TextView prefaceTitle = view.findViewById(R.id.new_preface_title);
        final EditText newPrefaceTitle = view.findViewById(R.id.title);
        final EditText newPrefaceText = view.findViewById(R.id.text);

        prefaceTitle.setText(!isUpdate ? "Add New Preface" : "Edit Preface");

        if (isUpdate && preface != null) {
            newPrefaceTitle.setText(preface.getTitle());
            newPrefaceText.setText(preface.getText());
        }

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(isUpdate ? "Update" : "Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("Delete",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {

                                if (isUpdate) {

                                    deletePreface(preface, position);
                                } else {

                                    dialogBox.cancel();

                                }

                            }
                        });


        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(newPrefaceTitle.getText().toString())) {
                    Toast.makeText(StudyMainActivity.this, "Enter preface name!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }


                if (isUpdate && preface != null) {

                    updatePreface(newPrefaceTitle.getText().toString(), newPrefaceText.getText().toString(), position);
                } else {

                    createPreface(newPrefaceTitle.getText().toString(), newPrefaceText.getText().toString());
                }
            }
        });
    }

    private void deletePreface(Preface preface, int position) {

        prefaceArrayList.remove(position);
        carnalitySeriesDatabase.getPrefaceDao().deletePreface(preface);
        prefaceAdapter.notifyDataSetChanged();
    }

    private void updatePreface(String title, String text, int position) {

        Preface preface = prefaceArrayList.get(position);

        preface.setTitle(title);
        preface.setText(text);

        carnalitySeriesDatabase.getPrefaceDao().updatePreface(preface);

        prefaceArrayList.set(position, preface);

        prefaceAdapter.notifyDataSetChanged();


    }

    private void createPreface(String title, String text) {

        long id = carnalitySeriesDatabase.getPrefaceDao().addPrefaceText(new Preface(0, title, text));


        Preface preface = carnalitySeriesDatabase.getPrefaceDao().getPrefaceEntry(id);

        if (preface != null) {

            prefaceArrayList.add(0, preface);
            prefaceAdapter.notifyDataSetChanged();

        }

    }
}
