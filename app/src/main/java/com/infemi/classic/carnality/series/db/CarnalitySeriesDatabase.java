package com.infemi.classic.carnality.series.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.infemi.classic.carnality.series.db.entity.Preface;

/**
 * Created by TheDancerCodes on 18/10/2018.
 */
@Database(entities = {Preface.class}, version = 1) // Represents all tables of the DB
public abstract class CarnalitySeriesDatabase extends RoomDatabase {

    // Define methods to get all DAOs
    public abstract PrefaceDao getPrefaceDao();

}
