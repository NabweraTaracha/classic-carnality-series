package com.infemi.classic.carnality.series.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by TheDancerCodes on 18/10/2018.
 */
@Entity(tableName = "preface")
public class Preface {

    @ColumnInfo(name = "preface_id")
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "preface_title")
    private String title;

    @ColumnInfo(name = "preface_text")
    private String text;

    @Ignore
    public Preface() {

    }


    public Preface(long id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;

    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
